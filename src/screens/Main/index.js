import React from 'react';

import Wrapper from './components/Wrapper';
import Container from './components/Container';
import Image from './components/Image';
import Controls from './components/Controls';

import useDogs from './useDogs';

const Main = () => {

  const { get, data } = useDogs();

  const handleSelectedBreed = ({ value }) => {
    get(value);
  };

  const handleRandomDog = () => {
    get();
  };

  return (
    <Wrapper>
      <Container>
        <Image source={data} />
        <Controls
          onChangeBreed={handleSelectedBreed}
          onClickToRandomize={handleRandomDog}
        />
      </Container>
    </Wrapper>
  );
};

export default Main;