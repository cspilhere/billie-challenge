import { useEffect, useState } from 'react';

import api from 'services/api';

const defaultValue = { empty: true };

const useDogs = (immediate) => {

  const [data, setData] = useState(defaultValue);
  const [status, setStatus] = useState('waiting');

  const get = async breed => {
    setStatus('loading');
    try {
      const response = await api.getDog(breed);
      setData(response);
      setStatus('success');
    } catch (error) {
      console.error('useDogs get error: ' + error);
      setStatus('error');
      return error;
    }
  };

  useEffect(() => {
    if (immediate) get();
  }, []);

  return {
    data,
    get,
    isEmpty: data.empty,
    isReady: status === 'success',
    status
  };

};

export default useDogs;
