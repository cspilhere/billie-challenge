import React, { useEffect, useState } from 'react';

import styled from 'styled-components';

import Select from 'react-select'

import useBreeds from '../useBreeds';

const ControlContainer = styled.div`
  width: 500px;
  display: flex;
  margin-top: 10px;
`;

const Button = styled.button`
  flex: 0;
  margin-left: 10px;
  border-radius: 4px;
  height: 38px;
  white-space: nowrap;
  outline: none;
`;

const Controls = ({ onChangeBreed, onClickToRandomize }) => {

  const [selectedBreed, setBreed] = useState(null);

  const { get, data: breeds, isReady } = useBreeds();

  useEffect(() => {
    get();
  }, []);

  return (
    <ControlContainer>
      <Select
        styles={{ container: () => ({ width: 500 }) }}
        menuPosition="fixed"
        loading={!isReady}
        options={breeds}
        value={selectedBreed}
        onChange={(selected) => {
          setBreed(selected);
          onChangeBreed(selected);
        }}
      />
      <Button
        onClick={() => {
          setBreed(null);
          onClickToRandomize();
        }}>
        Random breed
      </Button>
    </ControlContainer>
  );
};

export default Controls;