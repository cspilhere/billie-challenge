import React, { useEffect, useRef } from 'react';

import styled from 'styled-components';

const ImageContainer = styled.div`
  width: 500px;
  height: 500px;
`;

const colors = [
  '#8093FF',
  '#FF502C',
  '#FF9472',
  '#FF91FF'
];

const getColor = () => colors[Math.floor(Math.random()*colors.length)];

const ImageComponent = ({ source }) => {

  const canvasRef = useRef(null);

  const drawImage = (src, hideText) => {
    const canvas = canvasRef.current;
    canvas.height = 500;
    canvas.width = 500;
    const context = canvas.getContext('2d');
    const img = new Image();
    img.src = src;
    img.setAttribute('style', 'height: 500px; width: auto;');
    img.onload = function(){
      context.drawImage(img, 0, 0, img.width, img.height);
      if (!hideText) {
        context.font = '16px sans-serif';
        context.fillStyle = getColor();
        context.textAlign = 'top';
        context.textBaseline = 'left';
        context.fillText('We ❤️ our pups!', 20, 30);
      }
    }
  };

  useEffect(() => {
    if (!source.empty) drawImage(source);
    if (source.empty) drawImage('http://report.hsppr.org/images/numbers-09.png', true);
  }, [source]);

  return (
    <ImageContainer>
      <canvas ref={canvasRef} />
    </ImageContainer>
  );
};

export default ImageComponent;