import React, { StrictMode } from 'react';
import ReactDOM from 'react-dom';

import { Router } from '@reach/router';

import Main from 'screens/Main';

const App = () => <Router><Main path="/*" /></Router>;

ReactDOM.render(
  process.env.NODE_ENV !== 'production' ? <StrictMode><App /></StrictMode> : <App />,
  document.getElementById('root')
);
