import request from 'axios';

const apiUrl = 'https://dog.ceo/api';

export default {
  getDog: async breed => {
    let resource = 'breeds/image/random';
    if (breed) resource = `breed/${breed}/images/random`;
    try {
      const response = await request.get(`${apiUrl}/${resource}`);
      const data = response.data.message;
      return data;
    } catch (error) {
      console.error('request error: ' + error);
      return null;
    }
  },
  listBreeds: async breed => {
    let resource = 'breeds/list/all';
    try {
      const response = await request.get(`${apiUrl}/${resource}`);
      const data = response.data.message;
      const transformedResponse = Object.keys(data).map(item => ({
        label: item.charAt(0).toUpperCase() + item.slice(1),
        value: item,
        types: data[item]
      }));
      return transformedResponse;
    } catch (error) {
      console.error('request error: ' + error);
      return null;
    }
  }
};
