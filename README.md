# Billie Challenge
Billie Challenge is a dog picture generator for people who love dogs.

### How does it work?
Fetching random dog images from https://dog.ceo/api, but you can randomize by your preferred breed.

### Stack
- ReactJs
- Axios
- Firebase (Hosting only)
- react-select
- styled-components

### How to run on your machine
- Clone the repo
- Run `npm install`
- Run `npm start`
- To deploy just run `npm run deploy` (if you have access to the firebase project)

### Roadmap
- Make the design better;
- Write some tests;
- Make the images easily shareable;
- Offer a shortcut to download the image ;
- Add useful links of dog ONGs and promote donations for them;